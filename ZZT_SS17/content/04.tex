%!TEX root = ../ZZT_SS17.tex
\section{Die Ringe $\ZZ \diagup m\ZZ$}
\label{sec:4}

In diesem Kapitel beschäftigen wir uns mit Kongruenzen modulo $m$ für $m \in \NN, m > 1$. \marginnote{19.6.}

\begin{definition}[Eulersche $\varphi$-Funktion]
	\label{def:4.1}
	Für $m \in \NN, m > 1,$ sei $\varphi(m) := \# (\ZZ \diagup m \ZZ)^*$ die Anzahl der Einheiten des endlichen Rings $\ZZ \diagup m \ZZ$. \marginnote{[1]}
	Man nennt $\varphi$ die \Index{Eulersche $\varphi$-Funktion}.
\end{definition}

\begin{lemma}
	\label{lemma:4.1}
	Sei $m \in \NN, m \geq 1$ und $a \in \ZZ$.
	Dann ist $[a]_m$ genau dann eine Einheit in $\ZZ \diagup m\ZZ$, wenn gilt: $\ggT(a,m)=1$.
\end{lemma}

\begin{beweis}
	Es gilt:
	\begin{align*}
		[a]_m \text{ ist Einheit} \quad &\Leftrightarrow \quad \text{es gibt ein } b \in \ZZ \text{ mit } [a]_m \cdot [b]_m = [1]_m \\
		&\Leftrightarrow \quad \text{es gibt ein } b \in \ZZ \text{ mit } ab = 1+km \text{ für ein } k \geq 0 \\
		&\overset{\mathclap{\text{\ref{satz:2.7}}}}{\Leftrightarrow} \quad \ggT(a,m) \mid 1 \\
		&\Leftrightarrow \quad \ggT(a,m) = 1.
	\end{align*}
\end{beweis}

\begin{korollar}
	\label{kor:4.1}
	Für $m \in \NN, m > 1$ sei $S(m) := \{a \in \ZZ : 1 \leq a \leq m, \ggT(a,m) = 1\}$.
	Dann gilt $\varphi(m) = \# S(m)$.
\end{korollar}

\begin{thm}[Euler]
	\label{thm:4.2}
	Sei $m \in \NN, m > 1$. \marginnote{[2]}
	Sei $a \in \ZZ$ mit $\ggT(a,m) = 1$.
	Dann gilt
	\[
		a^{\varphi(m)} \kon 1 \modu{m}.
	\]
\end{thm}

\begin{beweis}
	Nach Lemma~\ref{lemma:4.1} gilt $[a]_m \in (\ZZ \diagup m\ZZ)^*$.
	Die Gruppe $(\ZZ \diagup m\ZZ^*, \cdot)$ hat $\varphi(m)$ Elemente, also teilt $d := \ord([a]_m)$ die Zahl $\varphi(m)$, das heißt $\varphi(m) = dk$ für ein $d \in \ZZ$. Es folgt
	\[
		[1]_m = [a]_m^{dk} = [a^{dk}]_m = [a^{\varphi(m)}]_m.
	\]
\end{beweis}

\begin{definition}[Körper]
	\label{erinnerung:4.3}
	Ein kommutativer Ring $(K,+,\cdot)$ heißt \Index{Körper}, wenn $K^* = K \setminus \setzero$ gilt. \marginnote{[3]}
\end{definition}

\begin{beispiel}
	\label{bsp:4.3}
	\begin{itemize}
		\item $(\ZZ,+,\cdot)$ ist kein Körper, denn $\ZZ^* = \{\pm 1\} \neq \ZZ \setminus \setzero$.
		\item $(\NN,+,\cdot)$ ist kein Ring, also auch kein Körper.
		\item $(\QQ,+,\cdot)$ und $(\RR,+,\cdot)$ sind Körper.
		\item Der Nullring $R = \setzero$ ist kein Körper, denn $R \setminus \setzero = \emptyset \neq R^* = R$.
	\end{itemize}
\end{beispiel}

\begin{satz}
	\label{satz:4.4}
	Sei $m \in \NN$. \marginnote{[4]}
	Dann sind äquivalent:
	\begin{enumerate}
		\item $\ZZ\diagup m\ZZ$ ist ein Körper.
		\item $m \in \PP$.
	\end{enumerate}
\end{satz}

\begin{beweis}
	Für $m = 0$ gilt $[a]_0 = \{a\}$ und $\ZZ \diagup 0 \ZZ$ ist genau wie $\ZZ$ kein Körper, da $(\ZZ \diagup 0 \ZZ)^* = \{[\pm 1]_0\}$.
	
	Für $m = 1$ gilt $\ZZ \diagup \ZZ = \{[0]_1\}$, das ist der Nullring und kein Körper.
	
	Ist $m \in \PP$, so gilt für alle $1 \leq a < m$, dass $\ggT(a,m) = 1$.
	Also ist $S(m) = \{1,\dots,m-1\}$ und damit
	\[
		(\ZZ \diagup m\ZZ)^* = \{[1]_m, \dots, [m-1]_m\} = \ZZ \diagup m\ZZ \setminus \{[0]_m\}.
	\]
	Ist $m \geq 2$ keine Primzahl, so gibt es ein $k$ mit $1 < k < m$ mit $k \mid m$, also $\ggT(k,m) = k \neq 1$.
	Es folgt
	\[
		[k]_m \notin (\ZZ \diagup m\ZZ)^* \neq \ZZ\diagup m\ZZ \setminus \{[0]_m\}.
	\]
\end{beweis}

Also gilt für $m \geq 1$:
\[
	\varphi(m) = m - 1 \quad \Leftrightarrow \quad m \in \PP.
\]

\[
	\begin{array}{ccc}
	\varphi(1) = 1 & \varphi(2) = 1 & \varphi(3) = 2 \\ 
	\varphi(4) = 2 & \varphi(5) = 4 & \varphi(6) = 2 \\ 
	\varphi(7) = 6 & \varphi(8) = 4 & \varphi(9) = 6
	\end{array} 
\]

\begin{korollar}[Satz von Fermat]
	\label{kor:4.5}
	Sei $p \in \PP$. \marginnote{[5]}
	Ist $a \in \ZZ$ mit $p \nmid a$, so gilt \index{Satz von Fermat}
	\[
		a^{p-1} \kon 1 \modu{p}.
	\]
	Für jedes $a \in \ZZ$ gilt $a^p \kon a \modu{p}$.
\end{korollar}

\begin{beweis}
	Die erste Behauptung ist ein Spezialfall von Theorem~\ref{thm:4.2}, da $\varphi(p) = p-1$.
	Es folgt dann auch $a^p \kon a \modu{p}$.
	
	Ist $p \mid a$, so ist $a \kon 0 \modu{p}$, also gilt auch in diesem Falle $a^p \kon a \modu{p}$.
\end{beweis}

\begin{definition}[Polynom, Polynomring]
	\label{erinnerung:4.6}
	Sei $R$ ein kommutativer Ring. \marginnote{[6]}
	Ein \Index{Polynom} mit Koeffizienten in $R$ in der Variablen $T$ ist eine Linearkombination
	\[
		f = a_nT^n + a_{n-1} T^{n-1} + \dots + a_0.
	\]
	Die $a_j \in R$ heißen \textbf{Koeffizienten} des Polynoms.
	Zwei Polynome sind gleich, wenn alle ihre Koeffizienten gleich sind.
	Ist $a_n \neq 0$, so heißt $a_n$ \Index{Leitkoeffizient} von $f$ und $n = \deg(f)$ der \Index{Grad} des Polynoms.
	Für das Nullpolynom $f = 0$ (alle Koeffizienten sind $0$) setzt man $\deg(0) = -\infty$.
	Die Menge aller Polynome mit Koeffizienten in $R$ in der Variablen $T$ bildet den kommutativen Ring mit der üblichen Addition und Multiplikation -- den \Index{Polynomring}:
	\[
		R[T] = \{a_nT^n + \dots + a_0 : n \geq 0, a_0,\dots,a_n \in R\}.
	\]
\end{definition}

\begin{lemma}
	\label{lemma:4.6}
	Sei $K$ ein Körper, sei $g,f \in K[T]$.
	Dann gilt:
	\begin{enumerate}[(i)]
		\item $\deg(f+g) \leq \max\{\deg(f),\deg(g)\}$
		\item $\deg(fg) = \deg(f) + \deg(g)$
	\end{enumerate}
\end{lemma}

\begin{beweis}
	Schreibe $f = a_mT^m + \dots + a_0$ und $g = b_nT^n + \dots + b_0$.
	\begin{enumerate}[(i)]
		\item Ist klar durch Aufschreiben der Koeffizienten von $f+g$.
		\item Sei $\deg(f) = m$ und $\deg(g) = n$, also $a_m, b_n \neq 0$.
		Der Leitkoeffizient von $fg = a_mb_nT^{m+n} + \dots + a_0b_0$ ist $a_m \cdot b_n \neq 0$, da $a_m,b_n \neq 0$.
	\end{enumerate}
\end{beweis}

\begin{bemerkung}
	(i) gilt auch in $R[T]$, wenn $R$ ein kommutativer Ring ist.
	Bei (ii) gilt dann aber nur \enquote{$\leq$}.
\end{bemerkung}

\begin{satz}[Teilen mit Rest]
	\label{satz:4.7}
	Sei $(K,+,\cdot)$ ein Körper und $f,g \in K[T]$ Polynome mit $g \neq 0$. \marginnote{[7] \\ 22.6.}
	Dann gibt es eindeutig bestimmte Polynome $h,r \in K[T]$ mit $\deg(r) < \deg(g)$ und $f = gh +r$.
\end{satz}

\begin{beweis}
	\textit{Zur Existenz:} Induktion nach $n = \deg(f)$.Für $n < \deg(g) = m$ ist nichts zu zeigen, setze $h = 0$ und $r = f$. Sei also $\deg(f) \geq \deg(g)$ und etwa $f = a_nT^n + \dots + a_0$, $g = b_mT^m + \dots + b_0$.
	Betrachte $\wt{f} = f-g \cdot \frac{a_n}{b_m} T^{n-m}$.
	Dann ist $\deg(\wt{f}) < n$, also existieren $\wt{h}, r \in K[T]$ mit $\wt{f} = g \cdot \wt{h} + r$ und $\deg(r) < m$.
	Es folgt
	\[
		f = \wt{f} + g \frac{a_n}{b_m} T^{n-m} = g \underbrace{\enb{\frac{a_n}{b_m} T^{n-m} + \wt{h}}}_{=:h} + r.
	\]
	\textit{Zur Eindeutigkeit:} Angenommen, $f = gh_1 + r_1 = gh_2 + r_2$ mit $\deg(r_1),\deg(r_2) < \deg(g)$.
	Dann gilt $g(h_1 - h_2) = r_2 - r_1$ und damit $m \cdot \deg(h_1 - h_2) = \deg(r_2 - r_1) < m$, also $h_1 - h_2 = 0$ und $r_1 = r_2$.
\end{beweis}

\begin{definition}[Wurzel, Nullstelle]
	\label{def:4.7}
	Ein Element $\lambda \in R$ heißt \Index{Wurzel} oder \Index{Nullstelle} des Polynoms $f = a_mT^m + \dots + a_0 \in R[T]$, wenn $f(\lambda) = a_m \lambda^m + \dots + a_0 = 0$ gilt.
\end{definition}

\begin{satz}
	\label{satz:4.8}
	Sei $(K,+,\cdot)$ ein Körper und sei $f \in K[T]$. \marginnote{[8]}
	Wenn $\lambda \in K$ eine Wurzel von $f$ ist, so gibt es genau ein $h \in K[T]$ mit $f = (T - \lambda)h$.
\end{satz}

\begin{beweis}
	Setze $g = T - \lambda \in K[T]$.
	Dann ist $\deg(g)  =1$.
	Division mit Rest liefert $f = gh + r$ mit $\deg(r) < 1$, also ist $r$ ein konstantes Polynom.
	Wegen
	\[
		\underbrace{f(\lambda)}_{=0} = \underbrace{g(\lambda) \cdot h(\lambda)}_{=0} + r(\lambda)
	\]
	folgt $r = 0$.
	Die Eindeutigkeit folgt aus dem vorigen Satz.
\end{beweis}

\begin{korollar}
	\label{kor:4.8}
	Ist $(K,+,\cdot)$ ein Körper und $f \in K[T]$ mit $\deg(f) = n \geq 0$, so hat $f$ höchstens $n$ verschiedene Wurzeln.
\end{korollar}

\begin{beweis}
	Seien $\lambda_1,\dots,\lambda_k$ Wurzeln von $f$, $\lambda_i \neq \lambda_j$ für $i \neq j$.
	Es folgt
	\[
		\begin{array}{rll}
		f = (T- \lambda_1) h_1, & 0 = f(\lambda_2) = \underbrace{(\lambda_2 - \lambda_1)}_{\neq 0} h_1(\lambda_2) & \Rightarrow h_1(\lambda_2) = 0 \\ 
		f = (T- \lambda_1)(T-\lambda_2) h_2, & 0 = f(\lambda_3) = \underbrace{(\lambda_3 - \lambda_1)(\lambda_3-\lambda_2}_{\neq 0} h_2(\lambda_3) & \Rightarrow h_2(\lambda_3) = 0 \\
		& \vdots & 
		\end{array} 
	\]
	also $f = (T-\lambda_1) \cdots (T-\lambda_k) h_k$ mit $h_k \neq 0$, und damit
	\[
		\deg(f) = k + \deg(h_k) \Rightarrow k \leq \deg(f).
	\]
\end{beweis}

\begin{thm}[Satz von Wilson]
	\label{thm:4.9}
	Sei $m \in \NN$ mit $m \geq 2$. \marginnote{[9]}
	Dann sind äquivalent:
	\begin{enumerate}[(i)]
		\item $m \in \PP$
		\item $(m-1)! \kon -1 \modu{m}$
	\end{enumerate}
\end{thm}

\begin{beweis}
	\begin{description}
		\item[(ii) $\Rightarrow$ (i):] Sei $k = (m-1)!$.
		Es gilt dann $k \cdot k \kon 1 \modu{m}$.
		Für $1 \leq a < m$ gilt $a \mid k$, also $k = ab$ für ein $b \in \NN$.
		Damit folgt $k^2 = a(ab^2) \kon 1 \modu{m}$, also ist $[a]_m$ eine Einheit.
		Nach Lemma~\ref{lemma:4.1} folgt $\ggT(m,a) = 1$.
		Da dies für alle $1 \leq a < m$ gilt, folgt $m \in \PP$.
		\item[(i) $\Rightarrow$ (ii):] Für $m \in \PP$ ist $\ZZ \diagup m\ZZ$ ein Körper nach Satz~\ref{satz:4.4}, also ist $(\ZZ \diagup m \ZZ)^* = \ZZ \diagup m\ZZ \setminus \{[0]_m\}$.
		Zu jedem $1 \leq a < m$ gibt es also genau ein $b$ mit $1 \leq b < m$ so, dass $ab \kon 1 \modu{m}$.
		Dabei gilt
		\[
			a = b \Leftrightarrow [a]_m^2 = [1]_m \xLeftrightarrow{\text{\ref{kor:4.8}}} [a]_m = \pm[1]_m \Leftrightarrow a \in \{1,m-1\}.
		\]
		Folglich gilt
		\begin{align*}
			2 \cdot 3 \cdot 4 \cdots (m-2) &\kon 1 \modu{m} \\
			1 \cdot 2 \cdot 3 \cdots (m-1) &\kon -1 \modu{m}.
		\end{align*}
	\end{description}
\end{beweis}

\begin{bemerkung}
	\label{bem:4.9}
	Als Primzahltest ist Wilsons Satz ungeeignet, da die Berechnung von $(m-1)!$ sehr rechenaufwendig ist.
\end{bemerkung}

\begin{korollar}
	\label{kor:4.9}
	Sei $p = 2l+1 \in \PP$ ungerade Primzahl.
	Dann gilt $(l!)^2 \kon (-1)^{l+1} \modu{p}$.
\end{korollar}

\begin{beweis}
	\begin{align*}
		\overbrace{1 \cdot 2 \cdot 3 \cdot \dots \cdot (p-1)}^{\kon -1 \modu{p}} &= 1 \cdot (p-1) \cdot 2 \cdot (p-2= \cdot \dots \cdot l \cdot(p-l) \\
		&\kon \underbrace{1 \cdot (-1) \cdot 2 \cdot (-2) \cdot \dots \cdot l \cdot (-l)}_{= (l!)^2 \cdot (-1)^l} \modu{p}
	\end{align*}
	Insgesamt also $(l!)^1 \kon (-1)^{l+1} \modu{p}$.
\end{beweis}

\begin{korollar}
	\label{kor:4.9.2}
	Ist $p \in \PP$ von der Form $p = 4k+1$, so gilt $((2k)!)^2 \kon -1 \modu{p}$, das heißt das Polynom $T^2 + 1$ hat eine Wurzel in $\ZZ \diagup p\ZZ$.
\end{korollar}

Um solche Wurzeln geht es im Folgenden.
Dazu brauchen wir einen Satz über zyklische Gruppen.

\begin{lemma}
	\label{lemma:4.A} \label{lemma:4.10}
	Sei $m \geq 1$. \marginnote{[10]}
	Für jedes $d \geq 1$ mit $d \mid m$ gibt es genau eine Untergruppe $A_d \subseteq \ZZ \diagup m\ZZ$ mit $d$ Elementen, nämlich
	\[
		A_d = \{[x]_m : d \cdot [x]_m = [0]_m\}.
	\]
\end{lemma}

\begin{beweis}
	Schreibe $m = d \cdot m'$.
	Nach Satz~\ref{satz:3.4} ist $A_d$ eine Untergruppe und nach Satz~\ref{satz:3.15} ist $A_d = \sk{[m']_m}$ zyklisch.
	Für $1 \leq s < d$ ist $s \cdot [m']_m = [s \cdot m']_m \neq [0]_m$ und $d \cdot [m']_m = [0]_m$, also $\#A_d = d = \ord([m']_m)$.
	
	Ist $H \subseteq \ZZ \diagup m\ZZ$ Untergruppe mit $d$ Elementen, so folgt für alle $[x]_m \in H$, dass $d \cdot [x]_m = [0]_m$, also $H \subseteq A_d$.
	Da $\#H = \#A_d = d$, folgt $H = A_d$.
\end{beweis}

\begin{lemma}
	\label{lemma:4.B} \label{lemma:4.10.2}
	Sei $(G,\cdot)$ eine Gruppe und $g \in G$ mit $\ord(g) = m < \infty$.
	Sei $k \geq 1$.
	Es gilt $\ord(g^k) = \ord(g)$ genau dann, wenn $\ggT(k,m) = 1$.
\end{lemma}

\begin{beweis}
	\begin{description}
		\item[\bewrueck] Wenn $\ggT(k,m) = 1$, dann existieren $u,v \in \ZZ$ mit $uk+vm = 1$. \marginnote{26.6.}
		Damit folgt
		\[
			g = g^1 = g^{uk+vm} = (g^k)^u \cdot (\underbrace{g^m}_{\mathclap{e}})^v = (g^k)^m \in \sk{g^k}.
		\]
		Also ist $\sk{g} \subseteq \sk{g^k}$.
		Es gilt auch $g^k \in \sk{g}$ und damit $\sk{g^k} \subseteq \sk{g}$.
		Insgesamt folgt $\sk{g} = \sk{g^k}$ und $\ord(g) = \ord(g^k)$.
		\item[\bewhin] Wenn $\ord(g^k) = \ord(g)$, so gilt $\sk{g^k} = \sk{g}$ und $g \in \sk{g^k}$.
		Es folgt $g^1 = (g^k)^u$ für ein $u \in \ZZ$, und mit $\ord(g) = m$ gilt
		$k \cdot u \kon 1 \modu{m}$.
		Mit Satz~\ref{satz:2.7} folgt $\ggT(k,m)=1$.
	\end{description}
\end{beweis}

\begin{lemma}
	\label{lemma:4.C} \label{lema:4.10.3}
	Sei $m \geq 1$.
	Dann gilt:
	\[
		m = \sum\limits_{\substack{d \geq 1 \\ d \mid m}} \varphi(d)
	\]
\end{lemma}

\begin{beweis}
	Wir setzen
	\begin{align*}
		\alpha(d) &\stack{}{:=} \# \{[a]_m \in \ZZ \diagup m\ZZ : \ord([a]_m) = d\} \\
		&\stack{\text{\ref{lemma:4.A}}}{=} \# \{[a]_m \in A_d : \ord([a]_m) = d\} \\
		&\stack{\text{\ref{lemma:4.B}}}{=} \varphi(d).
	\end{align*}
	Also:
	\[
		m = \#\ZZ \diagup m\ZZ = \sum\limits_{\substack{d \geq 1 \\ d \mid m}} \alpha(d) = \sum\limits_{\substack{d \geq 1 \\ d \mid m}} \varphi(d).
	\]
\end{beweis}

\begin{thm}
	\label{thm:4.10}
	Sei $(G,\cdot)$ eine endliche abelsche Gruppe mit $\#G = m$.
	Wenn es für jedes $d \geq 1$ mit $d \mid m$ höchstens $d$ Elemente $g \in G$ gibt mit $g^d = e$, so ist $G$ zyklisch.
\end{thm}

\begin{beweis}
	Setze $\alpha(d) = \#\{g \in G : \ord(g) = d\} \leq d$.
	Ist $g \in G$ mit $\ord(g) =d$, so gilt $d \geq 1$ und $d \mid m$.
	Für alle $g^k \in \sk{g}$ gilt dann $(g^k)^d = e$, also folgt $\sk{g} = \{x \in G : x^d = e\}$ nach Voraussetzung.
	Damit folgt
	\[
		m = \#G = \# \overset{\bullet}{\bigcup_{\substack{d \geq 1 \\ d \mid m}}} \{g \in G : \ord(g) = d\} = \sum_{\substack{d \geq 1 \\ d \mid m}} \alpha(d) \leq \sum_{d \geq 1 \\ d \mid m} \varphi(d) \stackrel{\text{\ref{lemma:4.C}}}{=} m
	\]
	und damit $\varphi(d) = \alpha(d)$ für alle $d \geq 1$ und $d \mid m$, insbesondere $\varphi(a) = \alpha(m) \geq 1$.
	Also existiert $x \in G$ mit $\ord(x) = m$, das heißt $\sk{x} = G$.
\end{beweis}

\begin{thm}
	\label{thm:4.11}
	Sei $(K,+,\cdot)$ ein Körper und $G \subseteq K^*$ eine Untergruppe von $(K^*,\cdot)$. \marginnote{[11]}
	Wenn $G$ endlich ist, dann ist $G$ zyklisch.
\end{thm}

\begin{beweis}
	Sei $g \in G$.
	Ist $g^k = 1$, so ist $g$ eine Wurzel von $T^k-1$.
	Nach Korollar~\ref{kor:4.8} gibt es höchstens $k$ verschiedene Wurzeln $T^k - 1$.
	Also können wir Theorem~\ref{thm:4.10} anwenden.
\end{beweis}

\begin{korollar}
	\label{kor:4.11}
	Ist $p \in \PP$, so ist die Einheitengruppe $(\ZZ \diagup p\ZZ)^*$ zyklisch.
	Insbesondere existiert also ein $\xi \in \ZZ$, sodass
	\[
		(\ZZ \diagup p\ZZ)^* = \{[1]_p,[2]_p,\dots,[p-1]_p\} = \{[\xi]_p, [\xi^2]_p,\dots,[\xi^{p-1}]_p\}.
	\]
	Man nennt $\xi$ eine \Index{Primitivwurzel} modulo $p$.
\end{korollar}

\begin{beispiel}
	\label{bsp:4.11}
	$2$ und $3$ sind zwei Primitivwurzeln modulo $5$, denn:
	\[
		\{ [2]_5, \underbrace{[2^2]_5}_{\mathclap{[4]_5}}, \underbrace{[2^3]_5}_{\mathclap{[3]_5}}, \underbrace{[2^4]_5}_{[1]_5} \} = (\ZZ \diagup 5\ZZ)^* = \{ [3]_5, \underbrace{[3^2]_5}_{[4]_5}, \underbrace{[3^3]_5}_{[2]_5}, \underbrace{[3^4]_5}_{[1]_5} \}
	\]
\end{beispiel}

\begin{satz}
	\label{satz:4.12}
	Sei $p \in \PP$ und $m \geq 1$ und sei $d = \ggT(m,p-1)$. \marginnote{[12]}
	Sei $a \in \ZZ$ mit $p \neq a$.
	Dann sind äquivalent:
	\begin{enumerate}[(i)]
		\item $T^m - [a]_p$ hat eine Wurzel, das heißt es gibt ein $x \in \ZZ$ mit $x^m \kon a \modu{p}$.
		\item $T^d - [a]_p$ hat eine Wurzel, das heißt es gibt ein $y \in \ZZ$ mit $y^d \kon a \modu{p}$.
		\item $a^{\frac{p-1}{d}} \kon 1 \modu{p}$.
	\end{enumerate}
\end{satz}

\begin{beweis}
	Schreibe $m = m' \cdot d$ und $p-1 = kd$.
	\begin{description}
		\item[(i) $\Rightarrow$ (ii):] Mit $y = x^{m'}$ gilt
		\[
			y^d = (x^{m'})^d = x^m \stackrel{\text{(i)}}{=} a \modu{p}.
		\] 
		\item[(ii) $\Rightarrow$ (iii):] Mit dem Satz von Fermat gilt
		\[
			a^{\frac{p-1}{d}} \stack{\text{(ii)}}{=} (y^d)^{\frac{p-1}{d}} = y^{p-1} \stackrel{\text{\ref{kor:4.5}}}{\kon} 1 \modu{p}.
		\]
		\item[(iii) $\Rightarrow$ (i):] Sei $\xi$ eine Primitivwurzel modulo $p$.
		Dann gibt es ein $s \geq 1$ mit $a \kon \xi^s \modu{p}$.
		Also folgt
		\begin{align*}
			&a^{\frac{p-1}{d}} \kon (\xi^s)^{\frac{p-1}{d}} = \xi^{sk} \stackrel{\text{(iii)}}{\kon} 1 \modu{p} \\
			\Rightarrow \quad &p-1 \kon kd \mid sk \\
			\Rightarrow \quad &d \mid s.
		\end{align*}
		Schriebe nun $s = s'd$.
		Es gibt $u,v \in \ZZ$ mit $d = um + v(p-1)$.
		Also erhalten wir
		\begin{align*}
			a \kon \xi^s &= \xi^{s'd} = \xi^{s' \cdot (um + v(p-1))} \\
			&= \xi^{s'um} \cdot \xi^{s'v(p-1)} \\
			&= \xi^{s'um} \cdot (\underbrace{\xi^{p-1}}_{\mathclap{\kon 1 \modu p}})^{s'v} \\
			&\kon (\xi^{s'u})^m \modu{p}
		\end{align*} 
		und damit
		\[
			(\underbrace{\xi^{s'u}}_{=: x})^m \kon a \modu{p}.
		\]
	\end{description}
\end{beweis}

\begin{beispiel}
	\label{bsp:4.12}
	Sei $p = 7$ und $m = d = 3$.
	Die Kongruenz $x^3 \kon a \modu{7}$ hat eine Lösung genau dann, wenn $a^2 \kon 1 \modu{7}$, das heißt $a \kon 1 \modu{7}$ oder $a \kon 6 \modu{7}$.
\end{beispiel}

Wir betrachten jetzt Quadratwurzeln modulo $p$, das heißt Lösungen von Kongruenzen $x^2 \kon a \modu{p}$.
Für $p=2$ ist das einfach.
Wir konzentrieren uns auf ungerade Primzahlen $p$.

\begin{definition}[Quadratisches Residuum, Legendre-Symbol]
	\label{def:4.13}
	Sei $p \in \PP$ mit $p \geq 3$. \marginnote{[13]}
	Eine Zahl $a \in \ZZ$ heißt \Index{quadratisches Residuum} modulo $p$, wenn $p \nmid a$ und wenn ein $x \in \ZZ$ existiert mit $x^2 \kon a \modu{p}$.
	
	Das \Index{Legendre-Symbol} ist definiert als
	\[
		\leg{a}{p} := \begin{cases}
			1, & \text{falls } x \in \ZZ \text{ existiert mit } x^2 \kon a \modu{p} \\
			-1, & \text{sonst.}
		\end{cases}
	\]
	Das Legendre-Symbol ist nur definiert, wenn $p \nmid a $ und $p \in \PP$ mit $p \geq 3$.
\end{definition}

\begin{satz}
	\label{satz:4.14}
	Sei $p \in \PP$ ungerade, $p = 2l+1$. \marginnote{[14] \\ 29.06.}
	Sei $a \in \ZZ$ mit $p \nmid a$.
	Dann gilt $a^l \kon \pm 1 \modu{p}$ sowie $\leg{a}{p} \kon a^l \modu{p}$.
\end{satz}

\begin{beweis}
	Für $b = a^l$ gilt nach Korollar~\ref{kor:4.5} $b^2 = a^{2l} = a^{p-1} \kon 1 \modu{p}$ und damit $a^l \kon \pm 1 \modu{p}$.
	Wende jetzt Satz~\ref{satz:4.12} an mit $m = 2 = d$ und $l = \frac{p-1}{2}$.
\end{beweis}

\begin{korollar}
	\label{kor:4.14}
	Sei $p \in \PP$ ungerade. Dann gilt
	\[
		\leg{-1}{p} = \begin{cases}
			1, & \text{falls } p \kon 1 \modu{4} \\
			-1, & \text{falls } p \kon 3 \modu{4}.
		\end{cases}
	\]
\end{korollar}

\begin{beweis}
	Schreibe $p = 2l+1$.
	Dann ist $l$ genau dann gerade, wenn $p \kon 1 \modu 4$, und genau dann gilt $(-1)^l \kon 1 \modu{p}$.
\end{beweis}

\begin{definition}[Gaußsche Menge]
	\label{def:4.15}
	Sei $p \in \PP$ ungerade, $p = 2l+1$. \marginnote{[15]}
	Eine Menge $\{u_1,\dots,u_l\} \subseteq \ZZ$ heißt \Index{Gaußsche Menge}, wenn gilt:
	\[
		(\ZZ \diagup p\ZZ)^* = \{ \pm [u_1]_p, \pm [u_2]_p, \dots, \pm [u_l]_p\}.
	\]
\end{definition}

Zum Beispiel ist $\{1,2,3,\dots,l\} \subseteq \ZZ$ eine Gaußsche Menge, denn $p-k \kon -k \modu{p}$.

\begin{lemma}[Gauß]
	Sei $p = 2l+1 \in \PP$ ungerade und $\{u_1,\dots,u_l\}$ eine Gaußsche Menge. \marginnote{[16]}
	Sei $a \in \ZZ$ mit $p \nmid a$.
	Wir definieren Zahlen $\varepsilon_1,\dots,\varepsilon_l \in \{\pm 1\}$ durch $au_i \kon \varepsilon_i u_j \modu{p}$.
	Dann gilt
	\[
		\leg{a}{p} = \varepsilon_1 \cdot \varepsilon_2 \cdot \dots \cdot \varepsilon_l.
	\]
\end{lemma}

\begin{beweis}
	Zu jedem $x \in \ZZ$ mit $p \nmid x$ gibt es genau ein $i$ so, dass $x \kon \pm u_i \modu{p}$, denn:
	\[
		\pm u_i \kon u_j \modu{p} \quad \Rightarrow \quad \#\{ \pm [u_1]_p, \dots, \pm [u_l]_p\} < 2l \quad \lightning
	\]
	Die $\varepsilon_i$ sind also wohldefiniert und eindeutig.
	Es gilt
	\begin{align*}
		(au_1) (au_2) \cdot \dots \cdot (au_l) = a^l u_1 \cdot \dots \cdot u_l &\kon \varepsilon_1 \cdot \dots \cdot \varepsilon_l u_1 \cdots u_l \modu{p} \\
		\Rightarrow \quad a^l &\kon \varepsilon_1 \cdot \dots \cdot \varepsilon_l \modu{p}
	\end{align*}
	Jetzt wenden wir Satz~\ref{satz:4.14} an.
\end{beweis}

\begin{satz}
	\label{satz:4.17}
	Sei $p \in \PP$ ungerade. \marginnote{[17]}
	Dann gilt
	\[
		\leg{2}{p} = \begin{cases}
			1, & \text{falls } p \kon 1,7 \modu{8} \\
			-1, & \text{falls } p \kon 3,5 \modu{8}.
		\end{cases}
	\]
\end{satz}

\begin{beweis}
	Schreibe $p = 2l+1$.
	Wir wenden Gauß' Lemma an mit $a = 2$ und der Gaußschen Menge $\{1,2,\dots,l\}$.
	\begin{description}
		\item[1. Fall:]  $l = 4k, 4k+1$.
			Dann gilt $\varepsilon_i = 1$ für $1 \leq i \leq 2k$, sonst $\varepsilon_i = -1$.
			Es folgt:
			\[
				\leg{2}{p} = \varepsilon_1 \cdots \varepsilon_l = (-1)^{l-2k} = (-1)^l =
				\left.\begin{cases}
					-1, & \text{falls } l = 4k+1 \\
					1, & \text{falls } l = 4k
				\end{cases} \right\} = \begin{cases}
					-1, & \text{falls } p \kon 3 \modu{8} \\
					1, & \text{falls } p \kon 1 \modu{8}.
				\end{cases}
			\]
		\item[2. Fall:] $l = 4k+2, 4k+3$.
			Dann gilt $\varepsilon_i = 1$ für $1 \leq i \leq 2k+1$ und sonst $\varepsilon_i = -1$.
			Es folgt
			\[
				\leg{2}{p} = \varepsilon_1 \cdots \varepsilon_l = (-1)^{l-2k-1} = (-1)^{l-1} = \left.\begin{cases}
					-1, & \text{falls } l = 4k+2 \\
					1, & \text{falls } l = 4k+3
				\end{cases} \right\} = \begin{cases}
					-1, & \text{falls } p \kon 5 \modu{8} \\
					1, & \text{falls } p \kon 7 \modu{8}.
				\end{cases}
			\]
	\end{description}
\end{beweis}

\begin{lemma}
	\label{lemma:4.18}
	Ist $p \in \PP$ ungerade und $\xi$ eine Primitivwurzel modulo $p$, so gilt \marginnote{[18]}
	\[
		\leg{\xi^s}{p} = (-1)^s.
	\]
\end{lemma}

\begin{beweis}
	\begin{align*}
		                      & \leg{\xi^s}{p} = 1                                              \\
		\Leftrightarrow \quad & \text{es gibt ein } t \text{ mit } \xi^s \kon \xi^{2t} \modu{p} \\
		\Leftrightarrow \quad & s-2t \kon 0 \modu{p-1} \text{ für geeignetes } t \in \ZZ        \\
		\Leftrightarrow \quad & s-2t = k \cdot (p-1) \text{ für geeignete } t,k \in \ZZ         \\
		\Leftrightarrow \quad & s \text{ gerade}.
	\end{align*}
\end{beweis}

\begin{korollar}
	\label{kor:4.18}
	Ist $p \in \PP$ ungerade und gilt $p \nmid a,b$, so gilt
	\[
		\leg{ab}{p} = \leg{a}{p} \cdot \leg{b}{p}.
	\]
\end{korollar}

\begin{beweis}
	Sei $\xi$ eine Primitivwurzel modulo $p$.
	Schreibe $a \kon\xi^s \modu{p}$ und $b \kon \xi^t \modu{p}$, also $ab \kon\xi^{s+t} \modu{p}$.
	Dann folgt
	\[
		\leg{ab}{p} = (-1)^{s+t} = (-1)^s \cdot (-1)^t = \leg{a}{p} \cdot \leg{b}{p}.
	\]
\end{beweis}

\begin{bemerkung}
	\label{bem:4.18}
	Wir können die Sätze \ref{satz:4.14} und \ref{satz:4.17} auch formulieren als
	\[
		\leg{-1}{p} = (-1)^{\frac{p-1}{2}} \qquad \qquad \qquad \leg{2}{p} = (-1)^{\frac{p^2-1}{8}},
	\]
	denn $\frac{p-1}{2}$ ist gerade genau dann, wenn $p \kon 1 \modu{4}$.
	
	$\frac{p^2-1}{8} = \frac{(p-1)(p+1)}{8}$ ist eine ganze Zahl und gerade genau dann, wenn $p \kon 1, 7 \modu{8}$.
\end{bemerkung}

Mit den Rechenregeln reduziert sich das Berechnen von Legendre-Symbolen auf den Fall $\leg{q}{p}$, wo $p,q \in \PP$ ungerade und $p \neq q$.
Für diesen Fall ist Gauß' quadratisches Reziprozitätsgesetz entscheidend.

\begin{thm}[Gaußsches Reziprozitätsgesetz]
	\label{thm:4.19}
	Sei $p,q \in \PP$ beide ungerade, $p \neq q$. \marginnote{[19]}
	Dann gilt \index{Gaußsches Reziprozitätsgesetz}
	\[
		\leg{q}{p} \cdot \leg{p}{q} = (-1)^{\frac{p-1}{2} \cdot \frac{q-1}{2}}
	\]
\end{thm}

\begin{beweis}
	Schreibe $p = 2n+1$ und $q = 2m+1$.
	Wir wenden Gauß' Lemma an auf $a = q$ mit Gaußscher Menge $\{1,2,\dots,n\}$.
	Für $1 \leq x \leq n$ schreibe
	\[
		qx \kon \varepsilon_x u \qquad u \in \{1,\dots,n\} \qquad \varepsilon_x = \pm 1.
	\]
	Dann ist $qx = \varepsilon_x u + py$ für ein $y \in \ZZ$, und es gilt $\varepsilon_x = -1$ genau dann, wenn $py = qx + u$.
	Wegen $u, x \geq 1$ folgt $y \geq 1$.
	Weiter gilt
	\[
		y = \frac{1}{p} (qx+u) \stack{x,u \leq n}{\leq} \frac{1}{p}(q+1)n \stack{\frac{n}{p}<\frac{1}{2}}{<} \frac{q+1}{2} = m+1,
	\]
	also $1 \leq y \leq m$.
	Ist umgekehrt $y \in \ZZ$ mit $1 \leq y \leq m$ und $1 \leq py - qx = u \leq n$, so folgt $\varepsilon_x = -1$.
	
	Sei $M = \{(x,y) : 1 \leq x \leq u, 1 \leq y \leq m\}$ und sei $A = \{(x,y) \in M : 1 \leq py - qx \leq n\}$.
	Es folgt $\leg{q}{p} = (-1)^{\#A}$.

	Setze $B = \{(x,y) \in M : 1 \leq qx-py \leq m\}$, dann gilt genauso $\leg{p}{q} = (-1)^{\#B}$.
	Für $1 \leq x \leq n$ gilt $\ggT(x,p) = 1$, also $py - qx \neq 0$.
	
	Es folgt $A \cup B = \{(x,y) \in M : -n \leq qx - py \leq m\}$.
	Da $A \cap B = \emptyset$ (nach Definition von $A$ und $B$), erhalten wir $\leg{q}{p} \cdot \leg{p}{q} = (-1)^{\#A \cup B}$.
	
	Seien
	\begin{align*}
		C &= \{(x,y) \in M : qx - py < -n\} \\
		D &= \{(x,y) \in M : qx - py > m\}.
	\end{align*}
	Dann gilt $M = A \overset{\cdot}{\cup} B \overset{\cdot}{\cup} C \overset{\cdot}{\cup} D$.
	Betrachte die Abbildung $\varphi(x,y) = (n+1-x,m+1-y)$.
	Für $(x,y) \in C$ gilt
	\[
		q(n+1-x) + p(m+1-y) = -(qx - py) + q \frac{p-1}{2} - p \frac{q-1}{2} = -(qx - py) + m-n > m,
	\]
	also $\varphi(C) \subseteq D$.
	Gleiches gilt für die Umkehrabbildung $\psi \colon D \rightarrow C$.
	Es folgt $\#C = \#D$ und damit
	\[
		(-1)^{mn} = (-1)^{\#A} \cdot (-1)^{\#B} \cdot \underbrace{(-1)^{\#C} \cdot (-1)^{\#D}}_{=1} = \leg{q}{p} \cdot \leg{p}{q}.
	\]
\end{beweis}

Mit dem Gaußschen Reziprozitätsgesetz lassen sich Legendre-Symbole sehr effektiv berechnen. \marginnote{3.7.}

\begin{beispiel}
	\label{bsp:4.19}
	\begin{enumerate}[(a)]
		\item Hat $x^2 \kon 17 \modu{101}$ eine Lösung?
		
		Es ist $101 \in \PP$ und $17 \in \PP$.
		Es gilt
		\[
			\leg{17}{101} \cdot \leg{101}{17} = (-1)^{\frac{100}{2} \cdot \frac{16}{2}} = 1,
		\]
		und da $101 \kon -1 \modu 17$, folgt
		\[
			\leg{17}{101} = \leg{101}{17} = \leg{-1}{17} \stack{\text{\ref{bem:4.18}}}{=} (-1)^\frac{16}{2} = 1.
		\]
		Also gibt es ein $x \in \ZZ$ mit $x^2 \kon 17 \modu{101}$.
		\item Hat $x^2 \kon 12 \modu{103}$ eine Lösung?
		
		Es ist $103 \in \PP$ und $12 = 3 \cdot 4$ und damit
		\[
			\leg{12}{103} \stack{\text{\ref{kor:4.18}}}{=} \leg{3}{103} \cdot \underbrace{\leg{4}{103}}_{=1} = \leg{3}{103}.
		\]
		Wegen
		\[
			\leg{3}{103} \cdot \leg{103}{3} = (-1)^{\frac{2}{2} \cdot \frac{102}{2}} = -1
		\]
		und $103 \kon 1 \modu{3}$ folgt
		\[
			\leg{12}{103} = \leg{3}{103} = - \leg{103}{3} = -\leg{1}{3} = -1.
		\]
		Also gibt es keine Lösungen der Kongruenz $x^2 \kon 12 \modu{103}$.
		\item Bestimme alle Primzahlen $p \in \PP$, für die $3$ ein quadratisches Residuum ist, das heißt für die es ein $x \in \ZZ$ gibt mit $x^2 \kon 3 \modu{p}$.
		
		Für $p=2$ ist $3 \kon 1^2 \modu{2}$, also erfüllt $p=2$ die Bedingung.
		Sei jetzt $p \geq 3$.
		\begin{description}
			\item[1. Fall:] $p \kon 1 \modu{4}$.
			Wir wollen $\leg{3}{p} = 1$.
			Es ist
			\[
				\leg{3}{p} \cdot \leg{p}{3} = (-1)^{\frac{2}{2} \cdot \frac{p-1}{2}} = 1,
			\]
			da $\frac{p-1}{2}$ gerade, und
			\[
				\leg{p}{3} = 1 \Leftrightarrow p \kon 1 \modu{3}.
			\]
			Gesucht sind also alle Primzahlen $p \in \PP$ mit $p \kon 1 \modu{4}$ und $p \kon 1 \modu{3}$.
			Schreibe $p = 4k+1$ und erhalte
			\begin{align*}
				p = 4k+1 &\kon 1 \modu{3} \\
				\Leftrightarrow \quad k+1 &\kon 1 \modu{3} \\
				\Leftrightarrow \quad k &\kon 0 \modu{3} \\
				\Leftrightarrow \quad k &= 3m,
			\end{align*}
			also $p = 12m + 1$.
			Für alle Primzahlen $p$ mit $p \kon 1 \modu{12}$ ist also $3$ ein quadratisches Residuum.
			\item[2. Fall:] $p \kon 3 \modu{4}$.
			Wir wollen wieder $\leg{3}{p} = 1$.
			Dieses Mal ist
			\[
				\leg{3}{p} \cdot \leg{p}{3} = (-1)^{\frac{2}{2} \cdot \frac{p-1}{2}} = -1,
			\]
			da $\frac{p-1}{2}$ ungerade, und
			\[
			\leg{p}{3} = -1 \Leftrightarrow p \kon 2 \modu{3}.
			\]
			Gesucht sind also alle Primzahlen $p \in \PP$ mit $p \kon 3 \modu{4}$ und $p \kon 2 \modu{3}$.
			Schreibe $p = 4k+3$ und erhalte
			\begin{align*}
			p = 4k+3 &\kon 2 \modu{3} \\
			\Leftrightarrow \quad k &\kon 2 \modu{3} \\
			\Leftrightarrow \quad k &= 3m+2,
			\end{align*}
			also $p = 12(m+1) = -1$.
			Für alle Primzahlen $p$ mit $p \kon -1 \modu{12}$ ist also $3$ ein quadratisches Residuum.
		\end{description}
		Insgesamt ist $3$ ein quadratisches Residuum modulo $p \in \PP$ genau dann, wenn $p = 2$ oder $p \kon \pm 1 \modu{12}$.
	\end{enumerate}
\end{beispiel}
\cleardoubleoddemptypage