%!TEX root = ../ZZT_SS17.tex
\section{Gruppen und Ringe}
\label{sec:3}

\begin{definition}[Gruppe]
	\label{def:3.1}
	Sei $G$ eine Menge und $\cdot \colon G \times G \rightarrow G, (a,b) \mapsto ab$ eine Abbildung (Verknüpfung). \marginnote{[1] \\ 18.5.}
	Wir nennen $(G,\cdot)$ eine \Index{Gruppe}, falls gilt:
	\begin{description}
		\item[(NE)] Es gibt ein $e \in G$ so, dass für alle $a \in G$ gilt: $a \cdot e = e \cdot a = a$.
		$e$ heißt ein Neutralelement von $G$.
		\item[(IV)] Zu jedem $a \in G$ gibt es ein $b \in G$ mit $a \cdot b = b \cdot a = e$.
		$b$ heißt ein Inverses zu $a$.
		\item[(AG)] Für alle $a,b,c \in G$ gilt $(a \cdot b) \cdot c = a \cdot (b \cdot c)$.
		Diese Eigenschaft heißt Assoziativität.  
	\end{description}
\end{definition}

\begin{beispiel}
	\label{bsp:3.1}
	\begin{enumerate}[(1)]
		\item $G = \ZZ$ mit der Verknüpfung $+$ ist eine Gruppe mit neutralem Element $e = 0$:
		\begin{gather*}
			a + 0 = 0 +a = a \\
			a + (-a) = (-a) + a = 0 \\
			(a+b) + c = a + (b+c)
		\end{gather*}
		\item $G = \ZZ$ mit Verknüpfung $\cdot$ ist keine Gruppe:
		\begin{gather*}
			a \cdot 1 = 1 \cdot a = a \\
			(a \cdot b) \cdot c = a \cdot (b \cdot c)
		\end{gather*}
		aber $2 \cdot b = 1$ hat keine Lösung in $\ZZ$, also gilt (IV) nicht.
		\item Die Vorzeichengruppe $C_2 = \{\pm 1\}$ mit Multiplikation ist eine Gruppe mit $e = 1$.
		\item $(\QQ,\cdot)$ ist keine Gruppe, denn es gibt keine Lösung der Gleichung $a \cdot b = 1$ für $a = 0$.
		\item $(\QQ^*,\cdot)$ mit $\QQ^* := \QQ \setminus \setzero$ ist eine Gruppe mit neutralem Element $e = 1$.
		Das Inverse zu $a = \frac{p}{q}$ ist $b = \frac{q}{p}$.
		\item $(\QQ,+)$ ist eine Gruppe.
	\end{enumerate}
\end{beispiel}

\begin{definition}[abelsch, kommutativ]
	\label{def:3.2}
	Eine Gruppe $(G,\cdot)$ heißt \Index{kommutativ} oder \Index{abelsch}, falls für alle $a,b \in G$ gilt \marginnote{[2]}
	\[
		a \cdot b = b \cdot a.
	\]
\end{definition}

Alle Beispiele oben sind abelsch.
Beispiel einer nicht abelschen Gruppe:
\[
	\Sym(3) := \{ f \colon \{1,2,3\} \rightarrow \{1,2,3\} : f \text{ bijektiv}\}
\]
mit der Komposition von Abbildungen als Verknüpfung.
Betrachte die Abbildungen
\[
	\pi \colon \begin{cases}
		1 \mapsto 2 \\
		2 \mapsto 3 \\
		3 \mapsto 1
	\end{cases} \qquad \qquad
	\rho \colon \begin{cases}
		1 \mapsto 2 \\
		2 \mapsto 1 \\
		3 \mapsto 3
	\end{cases}
\]
Dann gilt:
\[
	\pi \circ \rho \colon \begin{cases}
		1 \mapsto 2 \mapsto 3 \\
		2 \mapsto 1 \mapsto 2 \\
		3 \mapsto 3 \mapsto 1
	\end{cases} \qquad \qquad
	\rho \circ \pi \colon \begin{cases}
		1 \mapsto 2 \mapsto 1 \\
		2 \mapsto 3 \mapsto 3 \\
		3 \mapsto 1 \mapsto 2
	\end{cases}
\]
also $\pi \circ \rho \neq \rho \circ \pi$.

In der Zahlentheorie wird es vor allem um abelsche Gruppen gehen.

\begin{satz}
	\label{satz:3.3}
	Sei $(G,\cdot)$ eine Gruppe und $a,c \in G$. \marginnote{[3]}
	Dann gibt es genau ein $x \in G$ und genau ein $y \in G$ mit $a \cdot x = c$ und $y \cdot a = c$.
\end{satz}

\begin{beweis}
	Nach (IV) gibt es ein $b \in G$ mit $ab = ba = e$.
	für $x = bc$ und $y = cb$ folgt $ax = abc = ec = c$ und $ya = cba = ce = c$.
	Es bleibt die Eindeutigkeit zu zeigen:
	
	Angenommen, $ax = ax'$.
	Dann folgt $x = ex = bax = bax' = ex' = x'$. (Analog $ya = ya'$)
\end{beweis}

\begin{korollar}
	\label{kor:3.3.1}
	Sei $(G,\cdot)$ eine Gruppe.
	Zu jedem $a \in G$ gibt es genau ein $b \in G$ mit $ab = e = ba$.
	Man schreibt $b = a^{-1}$ und nennt $b$ das Inverse von $a$.
\end{korollar}

\begin{korollar}
	\label{kor:3.3.2}
	In Gruppen darf man kürzen:
	Aus $ac = bc$ folgt $a=b$, genauso folgt aus $ca = cb$, dass $a = b$.
	Denn: $ac = bc \Rightarrow acc^{-1} = bcc^{-1} \Rightarrow a=b$.
\end{korollar}

\begin{korollar}
	\label{kor:3.3.3}
	In jeder Gruppe gibt es nur ein einziges Element $e$ mit der Eigenschaft, dass $ae = a =ea$ für alle $a \in G$ gilt.
\end{korollar}

\textbf{Vorsicht:} Ist $G$ nicht abelsch, so folgt aus $ax = xb$ nicht unbedingt, dass $a=b$!

\begin{bemerkung}
	\label{bem:3.3.4}
	Wenn man das \enquote{$+$}-Zeichen für die Verknüpfung benutzt, dass schreibt man das Inverse von $a$ als $-a$ und nicht als $a^{-1}$.
	In dem Fall wird das Neutralelement $e$ als $0$ geschrieben, also
	\[
		a + (-a) = (-a) + a = a-a = 0.
	\]
	Das \enquote{$+$}-Zeichen wird nur bei abelschen Gruppen genutzt.
\end{bemerkung}

\begin{bemerkung}
	\label{bem:3.3.5}
	In jeder Gruppe $(G,\cdot)$ gilt $(a^{-1})^{-1} = a$ und $(ab)^{-1} = b^{-1}a^{-1}$, denn:
	\[
		ab(b^{-1}a^{-1}) = aea^{-1} = aa^{-1} = e = ab(ab)^{-1}
	\]
\end{bemerkung}

\begin{definition}[Untergruppe]
	\label{def:3.4}
	Sei $(G,\cdot)$ eine Gruppe mit neutralem Element $e$.
	Sei $H \subseteq G$ eine Teilmenge. \marginnote{[4] \\ 22.5.}
	Wir nennen $H$ eine \Index{Untergruppe} von $G$, falls gilt:
	\begin{enumerate}[(i)]
		\item $e \in H$
		\item Für alle $a,b \in H$ gilt $ab \in H$
		\item Für alle $a \in H$ gilt $a^{-1} \in H$.
	\end{enumerate}
\end{definition}

\begin{beispiel}
	\label{bsp:3.4}
	\begin{itemize}
		\item Die Gruppe $(\ZZ,+)$:
		Sei $d \in \NN$ und $H = d\ZZ= \{dz : z \in \ZZ\}$.
		Es gilt $0 \in H$ und für alle $a,b \in d\ZZ$ gilt $a+b \in d\ZZ$ und $-a \in d\ZZ$.
		\item Dagegen ist $\NN \subseteq \ZZ$ keine Untergruppe von $(\ZZ,+)$.
		Zwar gelten (i) und (ii) für $\NN$, aber (iii) gilt nicht, da zum Beispiel $-1 \notin \NN$.
	\end{itemize}
\end{beispiel}

\begin{satz}[Sparsames Kriterium für Untergruppen]
	\label{satz:3.4}
	Sei $(G,\cdot)$ eine Gruppe, sei $H \subseteq G$.
	Dann ist äquivalent:
	\begin{enumerate}[(i)]
		\item $H$ ist eine Untergruppe.
		\item $H \neq \emptyset$ und für alle $a,b \in H$ gilt $a^{-1}b \in H$.
	\end{enumerate}
\end{satz}

\begin{beweis}
	\begin{description}
		\item[(i) $\Rightarrow$ (ii):] Da $e \in H$, ist $e \neq \emptyset$.
		Für $a,b \in H$ folgt $a^{-1}, b \in H$ und damit $a^{-1}b \in H$.
		\item[(ii) $\Rightarrow$ (i):] Wähle $a \in H$ beliebig (möglich, da $H \neq \emptyset$).
		Es folgt $e = a^{-1} \cdot a \in H$.
		Damit ist auch $a^{-1} = a^{-1}e \in H$.
		Ist also $a,b \in H$, so ist auch $ab = (a^{-1})^{-1}b \in H$.
	\end{description}
\end{beweis}

\begin{bemerkung}
	\begin{itemize}
		\item In jeder Gruppe $G$ sind $\{e\}$ und $G$ Untergruppen.
		\item Jede Untergruppe $H \subseteq G$ ist ihrerseits eine Gruppe.
	\end{itemize}
\end{bemerkung}

\begin{thm}[Untergruppen von $\ZZ$]
	\label{thm:2.5}
	Die Untergruppen von $(\ZZ,+)$ sind genau die Mengen $d\ZZ$ für $d \in \NN$. \marginnote{[5]}
\end{thm}

\begin{beweis}
	In Beispiel~\ref{bsp:3.4} hatten wir schon überlegt, dass $d\ZZ$ immer eine Untergruppe ist.
	In Satz~\ref{satz:1.6} haben wir bewiesen:
	Ist $H \subseteq \ZZ$ mit $H \neq \emptyset$ und gilt für alle $a,b \in H$, dass $a-b \in H$, so folgt $H = d\ZZ$ für ein $d \in \NN$.
	Das ist genau das Kriterium (ii) aus Satz~\ref{satz:3.4}, additiv geschrieben.
\end{beweis}

\textbf{Erinnerung:} In Ausblick~\ref{aus:2.10} hatten wir Kongruenzklassen in $\ZZ$ definiert als $[a]_m = \{a+mt : t \in \ZZ\}$.
Wir erweitern das Konzept jetzt auf Gruppen.

\begin{definition}[Links- und Rechtsnebenklassen]
	\label{def:3.6}
	Sei $(G,\cdot)$ eine Gruppe und sei $H \subseteq G$ eine Untergruppe. \marginnote{[6]}
	Sei $a \in G$.
	Die \textbf{Linksnebenklasse} (bzw. \textbf{Rechtsnebenklassn}) von $a$ bezüglich $H$ ist die Menge \index{Nebenklasse}
	\[
		aH = \{ah : h \in H\} \subseteq G \qquad \text{bzw.} \qquad Ha = \{ha : h \in H\} \subseteq G.
	\]
	Die Menge aller Linksnebenklassen (bzw. Rechtsnebenklassen) ist
	\[
		G \diagup H = \{aH : a \in G\} \qquad \text{bzw.} \qquad H \diagdown G = \{Ha : a \in G\}
	\]
\end{definition}

\begin{bemerkung}
	\label{bem:3.6}
	Falls die Verknüpfung mit $+$ geschrieben wird, dann schreibt man $a+H$ anstelle von $aH$.
\end{bemerkung}

\begin{beispiel}
	\label{bsp:3.7}
	Betrachte die Gruppe $(\ZZ,+)$ und die Untergruppe $H = d\ZZ$ für ein $d \in \NN$. \marginnote{[7]}
	Die Linksnebenklasse von $a \in \ZZ$ bezüglich $a$ ist dann
	\[
		a + d\ZZ = \{a+dz : z \in \ZZ\} = [a]_d
	\]
	und stimmt genau mit der Kongruenzklasse von $a$ modulo $d$ überein.
	Kongruenzklassen sind also spezielle Nebenklassen.
\end{beispiel}

\begin{satz}[Eigenschaften von Nebenklassen]
	\label{satz:3.8}
	Sei $(G,\cdot)$ eine Gruppe und $H \subseteq G$ eine Untergruppe. \marginnote{[8]}
	Dann gilt für $a,b \in G$:
	\begin{enumerate}[(i)]
		\item $aH = bH \Leftrightarrow aH \cap bH \neq \emptyset \Leftrightarrow b \in aH$, das heißt \enquote{Nebenklassen sind gleich oder disjunkt}.
		\item Die Abbildung $H \rightarrow aH, h \mapsto ah$ ist eine bijektive Abbildung, das heißt \enquote{alle Nebenklassen sind gleich lang}.
	\end{enumerate}
\end{satz}

\begin{beweis}
	\begin{enumerate}[(i)]
		\item Angenommen, $c \in aH \cap bH$, also gibt es $h_1,h_2 \in H$ mit $c = ah_1 = bh_2$.
		Es folgt $b = ah_1h_2^{-1} = ah_3 \in aH$ mit $h_3 = h_1h_2^{-1}$.
		Daraus folgt
		\[
			bH = \{bh : h \in H\} = \{ah_3h : h \in H\} = \{a\wt{h} : \wt{h} \in H\} = aH.
		\]
		\item Sei $\lambda_a \colon H \rightarrow aH, h \mapsto ah$.
		Betrachte $\lambda_{a^{-1}}\colon aH \rightarrow H, ah \mapsto a^{-1}ah = h$.
		Es folgt $\lambda_{a^{-1}} \circ \lambda_a = \id_H$ und $\lambda_a \circ \lambda_{a^{-1}} = \id_{aH}$.
	\end{enumerate}
\end{beweis}

\textbf{Konvention:} Ist $M$ eine Menge, so ist $\#M$ die Anzahl der Elemente von $M$, falls $M$ endlich ist, und $\#M = \infty$, falls $M$ nicht endlich ist.
Es gilt $\#M = 0 \Leftrightarrow M = \emptyset$.

\begin{thm}[Satz von Lagrange]
	\label{thm:3.9}
	Sei $(G,\cdot)$ eine Gruppe und $H \subseteq G$ eine Untergruppe. \marginnote{[9]}
	Falls zwei der Zahlen $\#G, \#G\diagup H, \#H$ endlich sind, so ist es die dritte auch und es gilt dann \index{Satz von Lagrange}
	\[
		\#G = \#G \diagup H \cdot \#H.
	\]
\end{thm}

\begin{beweis}
	Wenn $\#G < \infty$, so gilt natürlich auch $\#G \diagup \#H < \infty$ und $\#H < \infty$ (jede Teilmenge einer endlichen Menge ist endlich und eine endliche Menge hat nur endlich viele Teilmengen).
	
	Angenommen, $\#G \diagup H$ und $\#H$ sind endlich.
	Jedes Element $a \in G$ liegt nach Satz~\ref{satz:3.8}(i) in genau einer Linksnebenklasse, nämlich $aH$.
	
	Jede Linksnebenklasse $aH$ hat genau $\#H$ Elemente nach Satz~\ref{satz:3.8}(ii).
	Folglich hat damit $G$ genau $H \cdot \# G \diagup H$ Elemente, also $\# G = \# G \diagup H \cdot \# H$.
\end{beweis}

Man schreibt $[G : H] := \#G \diagup H$ und nennt diese Zahl den \Index{Index} der Untergruppe $H$ in $G$. \marginnote{29.5.}
Damit schreibt sich der Satz von Lagrange als
\[
	\#G = [G : H] \cdot \#H.
\]

\begin{bemerkung}
	\label{bem:3.10}
	\begin{enumerate}[(1)]
		\item In den vorigen Sätzen \ref{satz:3.8} und \ref{thm:3.9} haben wir Linksnebenklassen betrachtet. \marginnote{[10]}
		Für Rechtsnebenklassen gelten entsprechende Aussagen.
		\item Ist $(G,\cdot)$ eine nicht-abelsche Gruppe, so gilt für $a \in G$ und $H \subseteq G$ Untergruppe im Allgemeinen $aH \neq Ha$.
		Allerdings ist $a \in aH \cap Ha$, also $aH \cap Ha \neq \emptyset$.
		Wenn $(G,\cdot)$ abelsch ist, so gilt dagegen $aH = Ha$, das heißt Rechts- und Linksnebenklassen stimmen überein.
	\end{enumerate}
\end{bemerkung}

\begin{lemma}
	\label{lemma:3.11}
	Sei $(G,\cdot)$ eine abelsche Gruppe und sei $H \subseteq G$ eine Untergruppe. \marginnote{[11]}
	Sei $a,b \in G$ und sei $a' \in aH, b' \in bH$.
	Dann gilt
	\[
		abH = a'b'H.
	\]
\end{lemma}

\begin{beweis}
	Schreibe $a' = ah_1$, $b' = bh_2$.
	Dann gilt
	\[
		a'b' = ah_1bh_2 = abh_1h_2 \in abH,
	\]
	also nach Satz~\ref{satz:3.8}(i) $a'b'H = abH$.
\end{beweis}

\begin{korollar}
	\label{kor:3.11}
	Ist $(G,\cdot)$ eine abelsche Gruppe, $H \subseteq G$ eine Untergruppe, so erhalten wir eine wohldefinierte Verknüpfung
	\begin{align*}
		\cdot \colon G \diagup H \times G \diagup H &\longrightarrow G \diagup H \\
		(aH,bH) &\longmapsto abH.
	\end{align*}
\end{korollar}

\begin{beweis}
	Das vorige Lemma zeigt: Wenn $aH = a'H$ und $bH = b'H$, so ist $a'b'H = abH$, das ist genau die Wohldefiniertheit der Verknüpfung.
\end{beweis}

\begin{thm}
	\label{thm:3.11}
	Sei $(G,\cdot)$ eine abelsche Gruppe und sei $H \subseteq G$ eine Untergruppe.
	Dann ist $G \diagup H$ mit der Verknüpfung $aH \cdot bH = abH$ eine abelsche Gruppe.
	Das Neutralelement ist $eH = H$, das Inverse von $aH$ ist $a^{-1}H$.
\end{thm}

\begin{beweis}
	Wir prüfen alle Axiome aus Definition~\ref{def:3.1}.
	Sei $e \in G$ das Neutralelement von $G$.
	\begin{enumerate}[(i)]
		\item Es gilt $eH = H$ und $eH \cdot aH = eaH = aH = aeH = aH \cdot eH$.
		\item Es gilt $aH \cdot a^{-1}H = aa^{-1}H = eH = H$.
		\item Für $ab,b,c \in G$ gilt $(aH \cdot bH) \cdot cH = abH \cdot cH = abcH = aH \cdot (bH \cdot cH)$.
		\item Für alle $a,b \in G$ gilt $ab=ba$, also auch $aH \cdot bH = abH = baH = bH \cdot aH$.
	\end{enumerate}
\end{beweis}

\begin{beispiel}
	\label{bsp:3.12}
	Die abelsche Gruppe $(\ZZ,+)$. \marginnote{[12]}
	Sei $m \in \NN$ und $H = m\ZZ$.
	Dann ist $H$ eine Untergruppe nach Beispiel~\ref{bsp:3.4}.
	Also bildet die Menge
	\[
		\ZZ \diagup m\ZZ = \{ [a]_m : a \in \ZZ\} = \{a + m\ZZ : a \in \ZZ\}
	\]
	aller Kongruenzklassen eine abelsche Gruppe mit Verknüpfung
	\[
		[a]_m + [b]_m = (a+ m\ZZ) + (b+m\ZZ) = (a+b)+m\ZZ = [a+b]_m.
	\]
\end{beispiel}

Wie groß sind diese Gruppen?

\begin{satz}
	\label{satz:3.12}
	Sei $m \in \NN$.
	Dann gilt
	\[
		\# \ZZ/m\ZZ = [\ZZ : m\ZZ] = \begin{cases}
			\infty, & \text{falls } m = 0\\
			m, & \text{falls } m > 0
		\end{cases}
	\]
\end{satz}

\begin{beweis}
	Für $m = 0$ gilt $[a]_0 = [b]_0 \Leftrightarrow a = b$, die Abbildung $\ZZ \rightarrow \ZZ \diagup 0\ZZ, a \mapsto [a]_0$ ist also bijektiv und damit ist $\ZZ \diagup 0\ZZ$ unendlich.
	
	Sei nun $m > 0$.
	Dann zeigt Teilen durch $m$ mit Rest:
	Für jedes $a \in \ZZ$ gibt es genau ein $r \in \NN$ mit $0 \leq r \leq m$ so, dass $a = ms + r$.
	Es gibt zu $a \in \ZZ$ also genau ein $r \in \NN$ mit $0 \leq r < m$ und $a \kon r \modu{m}$.
	Folglich gilt:
	\[
		\ZZ\diagup m\ZZ = \{ \underbrace{[0]_m, [1]_m, \dots, [m-1]_m}_{\mathclap{\text{genau } m \text{ verschiedene Nebenklassen bzw. Kongruenzklassen}}} \}
	\]
\end{beweis}

\begin{definition}
	\label{def:3.13}
	Sei $(G,\cdot)$ eine Gruppe und $g \in G$. \marginnote{[13]}
	Für $l \in \ZZ$ definieren wir
	\[
		g^l := \begin{cases}
			e, & \text{falls } l = 0 \\
			\underbrace{g \cdot \dots \cdot g}_{l\text{ Mal}}, & \text{falls } l > 0 \\
			\underbrace{g^{-1} \cdot \dots \cdot g^{-1}}_{-l\text{ Mal}}, & \text{falls } l < 0.
		\end{cases}
	\]
	Damit gilt $(g^l)^{-1} = g^{-l} = (g^{-1})^l$.
\end{definition}

\begin{lemma}
	\label{lemma:3.13}
	Mit dieser Konvention gilt für alle $k,l \in \ZZ$ die Formel
	\[
		g^k \cdot g^l = g^{k+l}.
	\]
\end{lemma}

\begin{beweis}
	Ist $k,l \geq 0$, so folgt das direkt aus der Definition.
	Ist $k,l \leq 0$, so gilt
	\[
		g^k \cdot g^l = (g^{-1})^{-l} \cdot (g^{-1})^{-k}
	\]
	und wir sind im vorigen Fall, das $-k,-l \geq 0$.
	Ist $k > 0$ und $l<0$, ao ist
	\[
		g^k \cdot g^l = \underbrace{g \cdot \dots \cdot g}_{k\text{ Mal}} \cdot \underbrace{g^{-1} \cdot \dots \cdot g^{-1}}_{-l\text{ Mal}} = g^{k+l} = g^{k+l}.
	\]
	Der Fall $k<0$ und $l > 0$ geht entsprechend.
\end{beweis}

\begin{bemerkung}
	\label{bem:3.13}
	Wenn die Verknüpfung als \enquote{$+$} geschrieben wird, so setzt man für $g \in G$ und $l \in \ZZ$:
	\[
		l \cdot g := \begin{cases}
		0, & \text{falls } l = 0 \\
		\underbrace{g + \dots + g}_{l\text{ Mal}}, & \text{falls } l > 0 \\
		\underbrace{(-g) + \dots + (-g)}_{-l\text{ Mal}}, & \text{falls } l < 0.
		\end{cases}
	\]
\end{bemerkung}

\begin{satz}[erzeugte zyklische Untergruppe]
	\label{satz:3.14}
	Sei $(G, \cdot)$ eine Gruppe, sei $g \in G$. \marginnote{[14] \\ 1.6.}
	Dann ist
	\[
		\sk{g} = \{ g^l : l \in \ZZ\} \subseteq G
	\]
	eine abelsche Untergruppe von $G$ -- die von $g$ \Index{erzeugte zyklische Untergruppe}.
	
	Ist $H \subseteq G$ eine Untergruppe und $g \in H$, so gilt $\sk{g} \subseteq H$.
\end{satz}

\begin{beweis}
	Wir benutzen Satz~\ref{satz:3.4}, um zu zeigen, dass $\sk{g} \subseteq G$ eine Untergruppe ist.
	Es gilt $g \in \sk{g}$, also $\sk{g} \neq \emptyset$.
	Für $k,l \in \ZZ$ ist $(g^k)^{-1} = g^{-k} \in \sk{g}$ und damit $(g^k)^{-1} \cdot g^l = g^{-k+l} \in \sk{g}$, also ist $\sk{g}$ eine Untergruppe.
	Wegen
	\[
		g^k \cdot g^l = g^{k+l} = g^{l+k} = g^{l} \cdot g^k
	\]
	ist $\sk{g}$ abelsch.
	
	Ist $g \in H$, so ist $g^0 = e \in H$ und $g^k = g \cdot \dots \cdot g \in H$ für alle $k >0$.
	Damit ist aber auch $g^{-k} = (g^k)^{-1} \in H$, also $\sk{g} \subseteq H$.
\end{beweis}

\begin{definition}[zyklische Gruppe]
	\label{def:3.15}
	Sei $(G,\cdot)$ eine Gruppe. \marginnote{[15]}
	Wir nennen $G$ \Index{zyklisch}, falls es ein $g \in G$ gibt mit $\sk{g} = G$.
\end{definition}

Beachte: Zyklische Gruppen sind stets abelsch, aber nicht jede abelsche Gruppe ist zyklisch.

\begin{beispiel}
	\label{bsp:3.15}
	\begin{itemize}
		\item $(\ZZ,+)$ ist zyklisch mit $\sk{1} = \ZZ$, denn für jedes $k \in \ZZ$ gilt $k = k \cdot 1$.
		\item $(d\ZZ,+)$ ist für jedes $d \in \ZZ$ zyklisch:
		Ist $k \in d\ZZ$, so gibt es ein $l \in \ZZ$ mit $k = ld$, also $d\ZZ = \sk{d} = \sk{-d}$
		\item $(\QQ,+)$ ist nicht zyklisch, denn es ist $\sk{0} = \{0\} \neq \QQ$ und für $a \in \QQ \setminus \setzero$ ist $\frac{1}{2} a \notin \sk{a} = \{ka : k \in \ZZ\}$, also gilt $\sk{a} \neq \QQ$.
	\end{itemize}
\end{beispiel}

\begin{satz}
	\label{satz:3.15}
	Jede Untergruppe einer zyklischen Gruppe ist zyklisch.
\end{satz}

\begin{beweis}
	Sei $(G,\cdot)$ zyklische Gruppe, sei $g \in G$ mit $\sk{g} = G$.
	Sei $H = \{e\}$, so gilt $H = \sk{e}$.
	
	Ist wiederum $H \neq \{e\}$, so gibt es ein $k \in \ZZ$, $k \neq 0$ mit $g^k \in H$.
	Dann gilt auch $(g^{k})^{-1} = g^{-k} \in H$.
	Wir setzen 
	\[
		n := \min\underbrace{\{k \in \NN : k \geq 1 \text{ und } g^k \in H\}}_{\neq \emptyset}
	\]
	sowie $h = g^n \in H$.
	Damit gilt $H = \sk{h}$:
	
	Sei $g^l \in H$ beliebig.
	Teilen durch $n$ mit Rest ergibt $l = nk + r$ mit $0 \leq r < n$ sowie
	\[
		g^l = g^{n\cdot k} \cdot g^r = (g^n)^k \cdot g^r = h^k \cdot g^r \in H,
	\]
	also auch $g^r \in h^{-1}H = H$.
	Es folgt $r = 0, g^r = e$ und damit $g^l = h^k \in \sk{h}$.
\end{beweis}

\begin{definition}[Ordnung]
	Sei $(G, \cdot)$ eine Gruppe, sei $g \in G$. \marginnote{[16]}
	Die \Index{Ordnung} von $g$ ist definiert als
	\[
		\ord(g) = \begin{cases}
			\infty, & \text{falls } g^k \neq e \text{ für alle } k \geq 1 \\
			\min\{k \in \NN: k \geq 1 \text{ und } g^k = e\}, & \text{sonst.}
		\end{cases}
	\]
\end{definition}

Also insbesondere gilt $\ord(g) = 1 \Leftrightarrow g = e$ und $\ord(g) = 2 \Leftrightarrow g \neq e$, aber $g^2 = e$.
Beachte auch $\ord(g) = \ord(g^{-1})$.

\begin{satz}
	\label{satz:3.16}
	Sei $(G,\cdot)$ eine Gruppe und sei $g \in G$.
	Dann gilt $\ord(g) = \#\sk{g}$.
\end{satz}

\begin{beweis}
	Sei $\ord(g) = \infty$.
	Dann gilt $g^k \neq g^l$ für alle $1 \leq k < l$, denn sonst wäre $g^{l-k} = e$ ein Widerspruch zu $\ord(g) = \infty$.
	Also enthält $\sk{g}$ die unendliche Menge $\{g^k : k \geq 1\}$ und es ist $\#\sk{g} = \infty$.
	
	Sei nun $\ord(g) = n < \infty$.
	Es gilt $g^k \neq g^l$ für alle $1 \leq k < l < n$, denn sonst wäre $g^{l-k} = e$ und $1 \leq l-k < n$ ein Widerspruch zu $\ord(g) = n$.
	Somit ist $\#\{g, g^2, \dots, g^n = g^0\} = n$.
	Für $l \in \ZZ$ beliebig gilt mit Teilen durch $n$ mit Rest, dass $g^l = g^{nk + r}, 0 \leq r < n$.
	Es folgt $g^l = (g^n)^k \cdot g^r = g^r \in \{g, g^2, \dots, g^n = g^0\}$, also $\sk{g} \subseteq \{g,g^2,\dots,g^n=g^0\} \subseteq \sk{g}$.
\end{beweis}

\begin{korollar}
	\label{kor:3.16}
	Ist $(G,\cdot)$ eine endliche Gruppe und $g \in G$, so gilt $\ord(g) \mid \#G$.
\end{korollar}

\begin{beweis}
	Da $\sk{g} \subseteq G$ ist $\sk{g}$ endlich, also $\ord(g) = \#\sk{g} < \infty$.
	Setze $H = \sk{g}$, dann gilt nach Theorem~\ref{thm:3.9}:
	\[
		\#G = [G : H] \cdot \#H = [G : H] \cdot \ord(g).
	\]
\end{beweis}

In $\ZZ$ haben wir zwei Verknüpfungen: \enquote{$+$} und \enquote{$\cdot$}.

\begin{definition}[Ring]
	\label{def:3.17}
	Ein \Index{Ring} $(R,+,\cdot)$ besteht aus einer Menge $R$ mit zwei Verknüpfungen \enquote{$+$} und \enquote{$\cdot$} mit folgenden Eigenschaften: \marginnote{[17] \\ 12.6.}
	\begin{description}
		\item[(R1)] $(R,+)$ ist eine abelsche Gruppe mit Neutralelement $0 \in R$.
		\item[(R2)] Für alle $a,b,c \in R$ gilt $a \cdot (b \cdot c) = (a \cdot b) \cdot c$ (Assoziativität der Multiplikation).
		\item[(R3)] Es gibt ein Einselement $1 \in R$, sodass $1 \cdot a = a = a \cdot 1$ für alle $a \in R$ gilt.
		\item[(R4)] Für alle $a,b,c \in R$ gelten die Distributivgesetze:
		\begin{align*}
			a \cdot (b+c) &= a \cdot b + a \cdot c \\
			(b+c) \cdot a &= b \cdot a + c \cdot a
		\end{align*}
		Konvention: Ohne Klammern gilt Punktrechnung vor Strichrechnung.
	\end{description}
	Falls zusätzlich $a \cdot b = b \cdot a$ für alle $a,b \in R$ gilt, heißt der Ring $R$ \textbf{kommutativ}.
\end{definition}

\begin{beispiel}
	\label{bsp:3.17}
	\begin{enumerate}[(i)]
		\item $(\ZZ,+,\cdot)$ ist ein kommutativer Ring.
		\item $(\QQ,+,\cdot)$ ist ein kommutativer Ring.
		\item $(\NN,+,\cdot)$ ist kein Ring, denn $(\NN,+)$ ist keine Gruppe.
		\item $\RR^{2 \times 2} = \penb{\begin{pmatrix}
				a & b \\ c & d
			\end{pmatrix} : a,b,c,d \in \RR}$ ist mit Matrizenmultiplikation und -addition ein nicht-kommutativer Ring.
		\item $R = \{0\}$ mit $0 \cdot 0 = 0 + 0 = 0$ ist ein Ring -- der \Index{Nullring}.
		In diesem Ring gilt $1 = 0$.
	\end{enumerate}
\end{beispiel}

\begin{lemma}[Rechenregeln in Ringen]
	\label{lemma:3.18}
	Sei $(R,+,\cdot)$ ein Ring. \marginnote{[18]}
	Dann gilt für alle $a,b \in R$:
	\begin{enumerate}[(i)]
		\item $a \cdot 0 = 0 = 0 \cdot a$
		\item $(-1) \cdot a = -a = a \cdot (-1)$
		\item $(-a) \cdot (-b) = a \cdot b$
	\end{enumerate}
\end{lemma}

\begin{beweis}
	\begin{enumerate}[(i)]
		\item $0 = 0 + 0$, also $a \cdot 0 = a \cdot (0+0) = a \cdot 0 + a \cdot 0 \Leftrightarrow a \cdot 0 = 0$.
		Analog folgt $0 \cdot a = 0$.
		\item $0 = 1-1$, also $0 = (1-1) \cdot a = 1 \cdot a + (-1) \cdot a = a + (-1) \cdot a \Leftrightarrow (-1) \cdot a = -a$.
		Analog folgt $a \cdot (-1) = a$.
		\item \begin{align*}
			0 &= (a-a)\cdot(b-b) = (a-a) \cdot b + (a-a) \cdot (-b) \\
			&= a \cdot b + (-a) \cdot b + a \cdot (-b) + (-a) \cdot (-b) \\
			&= a \cdot b + (-1) \cdot ab + (-1) \cdot ab + (-a) \cdot (-b) \\
			&= (ab - ab) - ab + (-a) \cdot (-b) \\
			\Leftrightarrow \quad ab &= (-a) \cdot (-b).
		\end{align*}
	\end{enumerate}
\end{beweis}

\begin{bemerkung}
	In \textbf{(R1)} hätten wir gar nicht verlangen müssen, dass \enquote{$+$} kommutativ ist, das folgt aus den Distributivgesetzen:
	\begin{align*}
		&(a+1) \cdot (b+1) = a \cdot (b+1) + 1 \cdot (b+1) = ab + a + b + 1 \\
		&(a+1) \cdot (b+1) = (a+1) \cdot b + (a+1) \cdot 1 = ab + b + a + 1 \\
		\Rightarrow \quad &a+b = b+a.
	\end{align*}
\end{bemerkung}

\begin{definition}[Einheit, Einheitengruppe]
	\label{def:3.19}
	Sei $(R,+,\cdot)$ ein Ring. \marginnote{[19]}
	Die \Index{Einheitengruppe} des Rings ist definiert als
	\[
		R^* := \{a \in R : \text{es existiert ein } b \in R \text{ mit } ab = 1 = ba\}.
	\]
	Es folgt $1 \in R^*$ und $(R^*,\cdot)$ ist eine Gruppe.
	Die Elemente von $R^*$ heißen \textbf{Einheiten}.\index{Einheit}
\end{definition}

\begin{beispiel}
	\begin{itemize}
		\item $\ZZ^* = \{\pm 1\}$.
		\item $\QQ^* = \QQ \setminus \setzero$
		\item Für den Nullring $R = \setzero$ ist $R^* = R$.
	\end{itemize}
\end{beispiel}

\begin{definition}[Ideal]
	\label{def:3.20}
	Sei $(R,+,\cdot)$ ein kommutativer Ring. \marginnote{[20]}
	Eine Teilmenge $I \subseteq \RR$ heißt \Index{Ideal}, falls gilt:
	\begin{description}
		\item[(I1)] $I$ ist eine Untergruppe von $(R,+)$ (insbesondere also $0 \in I$).
		\item[(I2)] Für alle $a \in R$ und $j \in I$ gilt $aj \in I$ (\enquote{absorbierende Eigenschaft}) 
	\end{description}
	Wir schreiben dann $I \NT R$.
\end{definition}

\begin{beispiel}
	\label{bsp:3.20}
	\begin{itemize}
		\item In jedem Ring $R$ sind $\setzero$ und $R$ Ideale -- die so genannten triviale Ideale.
		\item In $(\ZZ,+,\cdot)$ ist $I = d \cdot \ZZ, d \in \NN$, ein Ideal:
		Für $a \in \ZZ$ und $j \in d\ZZ$ gilt $j = kd$ für ein $k \in \ZZ$.
		Damit folgt $aj = akd \in d\ZZ$, also $d\ZZ \NT \ZZ$.
	\end{itemize}
\end{beispiel}

\begin{lemma}
	\label{lemma:3.20}
	Sei $(R,+,\cdot)$ ein kommutativer Ring, sei $I \NT R$ ein Ideal.
	Wenn $I \cap R^* \neq \emptyset$ gilt, so folgt $I = R$.
\end{lemma}

\begin{beweis}
	Angenommen, $a \in I \cap R^*$.
	Wähle $b \in R$ mit $ab = 1$, dann ist $ab = 1 \in I$.
	Für jedes $x \in R$ gilt daher $x = x \cdot 1 \in I$.
\end{beweis}

\begin{lemma}
	\label{lemma:3.21}
	Sei $(R,+,\cdot)$ ein kommutativer Ring und $I \NT R$ ein Ideal. \marginnote{[21]}
	Sind $a,a',b,b' \in R$ mit $a-a', b-b' \in I$, so folgt $ab - a'b' \in I$.
\end{lemma}

\begin{beweis}
	Schreibe $a = a' + j_1, b = b' + j_2$ mit $j_1,j_2 \in I$.
	Es folgt $ab - a'b' = a'b'+j_1b + aj_2 + j_1j_2 - a'b' \in I$.
\end{beweis}

\begin{thm}
	\label{thm:3.21}
	Sei $(R,+,\cdot)$ ein kommutativer Ring.
	Sei $I \NT R$ ein Ideal.
	Dann erhalten wir auf der Menge der Restklassen $R \diagup I = \{a + I : a \in R\}$ wohldefinierte Verknüpfungen \enquote{$+$} und \enquote{$\cdot$} durch
	\begin{align*}
		(a + I) + (b+I) &= (a+b)+I, \\
		(a+I) \cdot (b+I) &= (a \cdot b) + I,
	\end{align*}
	und $(R \diagup I, +, \cdot)$ ist wieder ein kommutativer Ring -- der \Index{Quotientenring} modulo $I$.
\end{thm}

\begin{beweis}
	Wir wissen schon aus Theorem~\ref{thm:3.11}, angewandt auf die Gruppe $(R,+)$ mit Untergruppe $I \subseteq R$, dass $(R\diagup I, +)$ eine abelsche Gruppe ist.
	
	Ist $a+I = a'+I$ und $b + I = b' + I$, so zeigt das vorige Lemma, dass $ab + I = a'b' + I$ gilt, also ist die Verknüpfung \enquote{$\cdot$} auf $R \diagup I$ wohldefiniert.
	Es gilt
	\begin{align*}
		(a + I) \cdot (1+I) = a \cdot 1 + I &= a + I = (1+ I) \cdot (a+I) \\
		((a+I) \cdot (b+I)) \cdot (c+ I) &= \dots = abc + I = \dots = (a+I) \cdot ((b+I) \cdot (c+I)) \\
		(a+I) \cdot (b+I) &= ab + I = ba + I = (b+I)\cdot(a+I)
	\end{align*}
	und analog die Distributivgesetze.
\end{beweis}

\begin{korollar}
	\label{kor:3.21}
	Für jedes $d \in \NN$ ist $(\ZZ \diagup d\ZZ,+,\cdot)$ ein kommutativer Ring.
\end{korollar}
\cleardoubleoddemptypage